#Lista 1 zadanie 2
#zbior miast
set Miasta;
#zbior typow
set typ;


#Stan dzwigow w poszczegolnych miastach kluczami sa miasta oraz typ dzwigow
param dzwigi{Miasta,typ}, integer;
#tablica 2 wymiarowa odleglosci miedzy miastami
param odleglosc{Miasta,Miasta}>=0;

#zmienne opisujace ilosc przetransportowanych dzwigow, dzwig I mozna zastapic II, kluczami sa miasta x miasta, pierwszy klucz opisuje z ktorego miasta jest wysylane a drugi klucz dokad, wartoscia tablica jest liczba okreslajaca ilosc przeslanych dzwigow
var zIdoI {Miasta,Miasta}>=0, integer;
var zIIdoII {Miasta,Miasta}>=0, integer;
var zIIdoI {Miasta,Miasta}>=0, integer;

#Ilosc wszystkich nadmiarowych dzwigow, oraz ilosc niedoboru 
param sumNiedoborI := sum{j in Miasta:dzwigi[j,"TypI"]<0}abs(dzwigi[j,"TypI"]);
param sumNiedoborII:= sum{j in Miasta:dzwigi[j,"TypII"]<0}abs(dzwigi[j,"TypII"]);
param sumNadmiarI  := sum{j in Miasta:dzwigi[j,"TypI"]>0}dzwigi[j,"TypI"];
param sumNadmiarII := sum{j in Miasta:dzwigi[j,"TypII"]>0}dzwigi[j,"TypII"];



#Celem jest minimalizacja kosztow transportu, koszt wynosi odleglosc oraz odleglosc*1.2 dla dzwigow typuII 
minimize Koszt_transportu: sum{i in Miasta, j in Miasta:i!=j}( zIdoI[i,j]*odleglosc[i,j]+1.2*zIIdoII[i,j]*odleglosc[i,j])+sum{i in Miasta, j in Miasta}(1.2*zIIdoI[i,j]*odleglosc[i,j]);



#Ilosc wyslanych dzwigow jest rowna maksymalnemu pokryciu zapotrzebowania z niedoborem, celem jest jak najwieksze zaspokojenie zapotrzebowania dzwigow
s.t. minChoose2: sum{i in Miasta, j in Miasta}(zIdoI[i,j]+zIIdoII[i,j]+zIIdoI[i,j]) = max(sumNadmiarII,sumNiedoborII)-abs(sumNadmiarII-sumNiedoborII)+ max(sumNiedoborI , if sumNadmiarII>=sumNiedoborII then sumNadmiarII+sumNadmiarI else sumNadmiarI) - abs(if sumNadmiarII>=sumNiedoborII then sumNadmiarII+sumNadmiarI-sumNiedoborI else sumNadmiarI-sumNiedoborI);

#max(sumNadmiarII,sumNiedoborII)-abs(sumNadmiarII-sumNiedoborII)=ilosc wyslanych dzwigow typuII
#abs(sumNadmiarII-sumNiedoborII)=ilosc dzwigow ktora zostanie w nadmiarze lub niedoborze po transporcie

#nie mozna wysylac tych samych dzwigow do siebie, ale mozna zastepowac I typu II typem
s.t. selfSending{i in Miasta}: zIdoI[i,i]+zIIdoII[i,i]=0;

#ilosc wyslanych mniejsza badz rowna nadmiarowi
s.t. minChooseWyslanezIdoI{i in Miasta}:sum{j in Miasta}zIdoI[i,j]<= max(0,dzwigi[i,"TypI"]);
s.t. minChooseWyslanezIIdoIiII{i in Miasta}:sum{j in Miasta}(zIIdoII[i,j]+zIIdoI[i,j])<= max(0,dzwigi[i,"TypII"]);

#ilosc odebranych mniejsza lub rowna ilosci niedoboru
s.t. minChooseReceivedzIdoIiII{j in Miasta}:sum{i in Miasta}(zIdoI[i,j]+zIIdoI[i,j])<= abs(min(0,dzwigi[j,"TypI"]));
s.t. minChooseReceivedzIIdoI{j in Miasta}:sum{i in Miasta}zIIdoII[i,j]<= abs(min(0,dzwigi[j,"TypII"]));

solve;
#Przedstawienie wynikow
display "Przeslanow dzwigow Typu I: ";
display{ j in Miasta, i in Miasta : zIdoI[i,j]>0}("z ",i," do ",j," wynosi", zIdoI[i,j]);

display "Przeslanow dzwigow Typu II: ";
display{ j in Miasta, i in Miasta : zIIdoII[i,j]>0}("z ",i," do ",j," wynosi", zIIdoII[i,j]);

display "Przeslanow dzwigow Typu II jako zamienniki dzwigow typu I: ";
display{ j in Miasta, i in Miasta : zIIdoI[i,j]>0}("z ",i," do ",j," wynosi", zIIdoI[i,j]);

display "Koszt transportu wynosi(zl): ",Koszt_transportu;


end;
