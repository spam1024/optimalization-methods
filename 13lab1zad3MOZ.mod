#Dawid Leśniak 

#ilosc okresow, wybrana wartosc jest minimalna do spelnienia warunkow
param t:=1497327;
#ilosc ropy B1
var B1 >=1,<=t, integer;
#ilosc ropy B2
var B2 >=1,<=t, integer;
/* KBx ilosc cykli krakowania tj zuzytych destylatow na krakowanie katalityczne */
var  KB1 >=0;
var  KB2 >=0;
/*wskaznik proporcji ilosc dostarczonego oleju do domowych paliw probBx i ciezkich paliw Bx-probBx */
var propB1,>=0;
var propB2, >=0;


#napotkane problemy: nie mozna mnozyc 2 zmiennych

#celem jest minimalizacja calkowitego kosztu produkcji
minimize  calkowity_koszt : 1300*B1 +1500*B2 + 10*B1 + 10*B2 + 20*KB1 + 20*KB2;


#minimalna ilosc paliwa silnikowego conajmniej 200 000t
s.t. paliwa_silnikowe: 0.15*B1+0.1*B2+0.5*KB1+0.5*KB2 >= 200000;

#domowe paliwa olejowe, czesc oleju idzie na produkcje ciezkich paliw, minimalna wyprodukowana ilosc to conajmniej 400 000t
s.t. domowe_paliwa_olejowe: 0.40*propB1 + 0.35*propB2 + 0.2*(KB1+KB2)>=400000;

#ciezkie paliwa olejowe:olej(dzielony jescze na domowe pal.)+destylat+resztki, conajmniej 250 000t
s.t. ciezkie_paliwa_olejowe: 0.40*(B1-propB1) + 0.15*(B1-KB1) + 0.15*B1 + 0.06*KB1 + 0.35*(B2-propB2) + 0.20*(B2-KB2) + 0.25*B2 + 0.06*KB2 >=250000;

#zawartosc siarki musi byc mniejsza niz 0.5%, przy czym zawartosc siarki w olejach pochadzacych z B1 wynosi 0.2%, 1.2% dla B2, 0.3% z krakowania dla B1 i 2,5% dla B2, Mnozymy wspolczynnik wydajnosci*ilosc cykli*% siarki i sumujemy cala zawartosc siarki
s.t. zawartosc_siarki: 0.40*propB1*2/1000 + 0.35*propB2*12/1000 + 0.2*KB1*3/1000 +0.2*KB2*25/1000  <= (5/1000)*(0.40*propB1 + 0.35*propB2 + 0.2*(KB1+KB2));

#ilosc skrakowanej ropy KBx musi byc mniejsza od ilosci wyprodukowanej ropy Bx
s.t. ilosc_krakowanaB1: KB1<=0.15*B1;
s.t. ilosc_krakowanaB2: KB2<=0.2*B2;
#ilosc oleju przekazanego na produkcje domowego paliwa olejowego musi byc mniejsza od ilosc wyprodukowanego oleju
B1ilosc_oleju_na_domowe: propB1<=0.4*B1;
B2ilosc_oleju_na_domowe: propB2<=0.35*B2;

solve;
display "Koszty produkcji w dolarach",calkowity_koszt;
end;
