#Dawid Leśniak
#magazynowanie nietkniete jeszcze

#ilosc produktow
param K>=1;
#ilosc okresow
param T>=2;

#zbior detali
set k:=1..K;
#zbior okresow
set t:=1..T;
#maksymalna ilosc produkcji produktu k w okresie t
param p{k,t},integer;
#kosz produkcji produktu k w okresie t
param c{k,t},integer;
#koszt rozpoczecia produkcji k w okresie t
param f{k,t},integer;
#zadania k produktow w okresie t
param d{k,t},integer;
#koszt magazynowania produktu k w okresie t
param h{k,t},integer;
# czy magazynowac i ile
var magazyn{k,t} >=0,integer;
#produkcja w danym czasie

#ile produktow produkowanych w danym okresie
var produkcja{k,t} >=0,integer;
#ktory produkt produkowany w danym okresie
var produkcja2{k,t},binary;
#kiedy rozpoczeto produkcje, jesli zaczetu produkowac w danym okresie t produkt k wartosc produkcja3[k,t]=1
var produkcja3{k,t},binary;

#minimalizacja kosztow produkcji przy zaspokojeniu potrzeb
#bez magazynowania
minimize koszt:sum{i in k, j in t} (produkcja[i,j]*c[i,j]+f[i,j]*produkcja3[i,j]+magazyn[i,j]*h[i,j]);
#produkcja zaspokaja potrzeby ilosc wyprodukowana w okresie t+ilosc zmagazynowana
s.t. zaspokojenie_potrzeb{i in k, j in t}:produkcja[i,j]+sum{j2 in t:j2<=j}magazyn[i,j2]>=d[i,j];
#mozna produkowac co najwyzej Pkt
s.t. przekraczanie_mozliwosci_produkcyjnych{i in k, j in t}:produkcja[i,j]<=produkcja2[i,j]*p[i,j];
#w danym okresie produkowany moze byc tylko jeden produkt
s.t. jeden_produkt_na_okres{j in t}:sum{i in k}produkcja2[i,j]<=1;
#warunek ktory wypelnia tablice okreslajaca czas rozpoczecia produkcji produktu l
s.t. A{i in k, j in 2..T}:produkcja3[i,j]>=produkcja2[i,j]-produkcja2[i,j-1];
s.t. B{i in k, j in 2..T}:2*produkcja3[i,j]-1<=produkcja2[i,j]-produkcja2[i,j-1];
s.t. C{i in k}:produkcja3[i,1]=produkcja2[i,1];
# ilosc zmagazynowana
s.t. zmagazynowano_pierwszy_okres{i in k}:magazyn[i,1]=produkcja[i,1]-d[i,1];
s.t. zmagazynowano{i in k, j in 2..T}:magazyn[i,j]= magazyn[i,j-1] + produkcja[i,j] - d[i,j];
#na koniec okresu nie powinno nic zalegac w magazynie
s.t. zmagazynowano2:sum{i in k}magazyn[i,T]<=0;
data;
param K:=5;
param T:=10;

#maksymalna ilosc produkcji produktu k w okresie t
param p:1	2	3	4	5	6	7	8	9	10:=
1	300 	33	40	7	75	84	123	35	65	75
2	100 	610	30	54	198	5	5	54	324	654
3	654	54	654	65	654	543	244	5	3	1
4	65	654	3	645	654	24	23	53	534	54
5	1	86	62	324	666	333	54	2	1	1	
;
#kosz produkcji produktu k w okresie t
param c:1	2	3	4	5	6	7	8	9	10:=
	1	41	22	34	43	32	54	95	56	25	30
	2	23	54	45	15	32	48	2	95	55	45
	3	23	21	54	62	2	65	1	33	22	12	
	4	21	32	54	84	2	21	98	35	45	32	
	5	23	54	48	68	78	98	78	26	54	1000
;
#koszt rozpoczecia produkcji k w okresie t
param f:1	2	3	4	5	6	7	8	9	10:=
	1	22	3	6	24	23	52	23	34	23	52
	2	22	5	22	23	42	23	52	21	23	52
	3	22	23	23	25	62	22	23	42	23	52
	4	23	24	52	231	24	52	23	24	52	1
	5	23	52	23	42	22	23	23	52	25	22
;
#zadania k produktow w okresie t
param d:1	2	3	4	5	6	7	8	9	10:=
	1	3	42	13	22	1	32	13	2	41	11
	2	0	12	33	25	85	65	35	32	23	56
	3	0	0	42	52	23	12	42	52	23	32
	4	0	0	0	24	52	1	2	3	13	0
	5	0	0	0	0	52	23	23	42	42	52
;
#koszt magazynowania produktu k w okresie t
param h:1	2	3	4	5	6	7	8	9	10:=
	1	2	2	3	4	4	6	5	4	5	3
	2	2	2	2	1	3	5	4	5	4	5
	3	3	5	3	2	4	1	3	5	4	6
	4	6	5	4	3	3	5	4	5	3	6
	5	5	3	4	3	5	4	3	5	6	1
;
end;
