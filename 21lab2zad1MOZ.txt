Problem:    21lab2zad1MOZ
Rows:       4
Columns:    41 (41 integer, 0 binary)
Non-zeros:  114
Status:     INTEGER OPTIMAL
Objective:  ilosc_straconych_cali = 18 (MINimum)

   No.   Row name        Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 ilosc_straconych_cali
                                1628                             
     2 zamowionie_7_calowych
                                 111           110               
     3 zamowionie_5_calowych
                                 121           120               
     4 zamowionie_3_calowych
                                  82            80               

   No. Column name       Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 x[1]         *             37             0           120 
     2 x[2]         *             28             0           120 
     3 x[3]         *              9             0           120 
     4 x[4]         *              0             0           120 
     5 x[5]         *              0             0           120 
     6 x[6]         *              0             0           120 
     7 x[7]         *              0             0           120 
     8 x[8]         *              0             0           120 
     9 x[9]         *              0             0           120 
    10 x[10]        *              0             0           120 
    11 x[11]        *              0             0           120 
    12 x[12]        *              0             0           120 
    13 x[13]        *              0             0           120 
    14 x[14]        *              0             0           120 
    15 x[15]        *              0             0           120 
    16 x[16]        *              0             0           120 
    17 x[17]        *              0             0           120 
    18 x[18]        *              0             0           120 
    19 x[19]        *              0             0           120 
    20 x[20]        *              0             0           120 
    21 x[21]        *              0             0           120 
    22 x[22]        *              0             0           120 
    23 x[23]        *              0             0           120 
    24 x[24]        *              0             0           120 
    25 x[25]        *              0             0           120 
    26 x[26]        *              0             0           120 
    27 x[27]        *              0             0           120 
    28 x[28]        *              0             0           120 
    29 x[29]        *              0             0           120 
    30 x[30]        *              0             0           120 
    31 x[31]        *              0             0           120 
    32 x[32]        *              0             0           120 
    33 x[33]        *              0             0           120 
    34 x[34]        *              0             0           120 
    35 x[35]        *              0             0           120 
    36 x[36]        *              0             0           120 
    37 x[37]        *              0             0           120 
    38 x[38]        *              0             0           120 
    39 x[39]        *              0             0           120 
    40 x[40]        *              0             0           120 
    41 x[41]        *              0             0           120 

Integer feasibility conditions:

KKT.PE: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

KKT.PB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

End of output
