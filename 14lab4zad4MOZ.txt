Problem:    14lab4zad4MOZ
Rows:       54
Columns:    20 (20 integer, 20 binary)
Non-zeros:  86
Status:     INTEGER OPTIMAL
Objective:  max_preferencje = 34 (MAXimum)

   No.   Row name        Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 max_preferencje
                                  34                             
     2 max4h_dziennie[poniedzialek]
                                 3.5                           4 
     3 max4h_dziennie[wtorek]
                                   3                           4 
     4 max4h_dziennie[sroda]
                                   2                           4 
     5 max4h_dziennie[czwartek]
                                   2                           4 
     6 max4h_dziennie[piatek]
                                   0                           4 
     7 obiad[poniedzialek]
                                   0            -0             = 
     8 obiad[wtorek]
                                   0            -0             = 
     9 obiad[sroda]                0            -0             = 
    10 obiad[czwartek]
                                   0            -0             = 
    11 obiad[piatek]
                                   0            -0             = 
    12 obiad2[poniedzialek]
                                   0                           1 
    13 obiad2[wtorek]
                                   0                           1 
    14 obiad2[sroda]
                                   0                           1 
    15 obiad2[czwartek]
                                   0                           1 
    16 obiad2[piatek]
                                   0                           1 
    17 przedmiot_tylko_raz[algebra]
                                   1             1             = 
    18 przedmiot_tylko_raz[analiza]
                                   1             1             = 
    19 przedmiot_tylko_raz[fizyka]
                                   1             1             = 
    20 przedmiot_tylko_raz[chemia_mineralow]
                                   1             1             = 
    21 przedmiot_tylko_raz[chemia_organiczna]
                                   1             1             = 
    22 nakladanieGodzin[1,1,algebra,analiza]
                                   0            -0             = 
    23 nakladanieGodzin[1,1,analiza,algebra]
                                   0            -0             = 
    24 nakladanieGodzin[1,1,chemia_mineralow,algebra]
                                   0            -0             = 
    25 nakladanieGodzin[1,1,chemia_mineralow,analiza]
                                   0            -0             = 
    26 nakladanieGodzin[1,1,chemia_mineralow,chemia_organiczna]
                                   0            -0             = 
    27 nakladanieGodzin[1,1,chemia_organiczna,algebra]
                                   0            -0             = 
    28 nakladanieGodzin[1,1,chemia_organiczna,analiza]
                                   0            -0             = 
    29 nakladanieGodzin[1,2,fizyka,algebra]
                                   0            -0             = 
    30 nakladanieGodzin[1,2,fizyka,analiza]
                                   0            -0             = 
    31 nakladanieGodzin[1,2,chemia_mineralow,chemia_organiczna]
                                   0            -0             = 
    32 nakladanieGodzin[2,1,chemia_mineralow,algebra]
                                   0            -0             = 
    33 nakladanieGodzin[2,1,chemia_mineralow,analiza]
                                   0            -0             = 
    34 nakladanieGodzin[2,1,chemia_mineralow,chemia_organiczna]
                                   0            -0             = 
    35 nakladanieGodzin[2,1,chemia_organiczna,algebra]
                                   0            -0             = 
    36 nakladanieGodzin[2,1,chemia_organiczna,analiza]
                                   0            -0             = 
    37 nakladanieGodzin[2,2,algebra,analiza]
                                   0            -0             = 
    38 nakladanieGodzin[2,2,algebra,fizyka]
                                   0            -0             = 
    39 nakladanieGodzin[2,2,analiza,algebra]
                                   0            -0             = 
    40 nakladanieGodzin[2,2,analiza,fizyka]
                                   0            -0             = 
    41 nakladanieGodzin[2,2,fizyka,algebra]
                                   0            -0             = 
    42 nakladanieGodzin[2,2,fizyka,analiza]
                                   0            -0             = 
    43 nakladanieGodzin[2,2,chemia_mineralow,chemia_organiczna]
                                   0            -0             = 
    44 nakladanieGodzin[3,3,algebra,analiza]
                                   0            -0             = 
    45 nakladanieGodzin[3,3,chemia_mineralow,fizyka]
                                   0            -0             = 
    46 nakladanieGodzin[3,4,analiza,algebra]
                                   0            -0             = 
    47 nakladanieGodzin[3,4,chemia_mineralow,fizyka]
                                   0            -0             = 
    48 nakladanieGodzin[3,4,chemia_organiczna,chemia_mineralow]
                                   0            -0             = 
    49 nakladanieGodzin[4,3,algebra,analiza]
                                   0            -0             = 
    50 nakladanieGodzin[4,3,analiza,fizyka]
                                   0            -0             = 
    51 nakladanieGodzin[4,3,analiza,chemia_mineralow]
                                   0            -0             = 
    52 nakladanieGodzin[4,4,analiza,fizyka]
                                   0            -0             = 
    53 nakladanieGodzin[4,4,chemia_mineralow,chemia_organiczna]
                                   0            -0             = 
    54 nakladanieGodzin[4,4,chemia_organiczna,chemia_mineralow]
                                   0            -0             = 

   No. Column name       Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 wybor[1,algebra]
                    *              0             0             1 
     2 wybor[1,analiza]
                    *              0             0             1 
     3 wybor[1,fizyka]
                    *              1             0             1 
     4 wybor[1,chemia_mineralow]
                    *              0             0             1 
     5 wybor[1,chemia_organiczna]
                    *              0             0             1 
     6 wybor[2,algebra]
                    *              0             0             1 
     7 wybor[2,analiza]
                    *              0             0             1 
     8 wybor[2,fizyka]
                    *              0             0             1 
     9 wybor[2,chemia_mineralow]
                    *              1             0             1 
    10 wybor[2,chemia_organiczna]
                    *              1             0             1 
    11 wybor[3,algebra]
                    *              1             0             1 
    12 wybor[3,analiza]
                    *              0             0             1 
    13 wybor[3,fizyka]
                    *              0             0             1 
    14 wybor[3,chemia_mineralow]
                    *              0             0             1 
    15 wybor[3,chemia_organiczna]
                    *              0             0             1 
    16 wybor[4,algebra]
                    *              0             0             1 
    17 wybor[4,analiza]
                    *              1             0             1 
    18 wybor[4,fizyka]
                    *              0             0             1 
    19 wybor[4,chemia_mineralow]
                    *              0             0             1 
    20 wybor[4,chemia_organiczna]
                    *              0             0             1 

Integer feasibility conditions:

KKT.PE: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

KKT.PB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

End of output
