Problem:    test
Rows:       57
Columns:    35 (21 integer, 21 binary)
Non-zeros:  154
Status:     INTEGER OPTIMAL
Objective:  OBJ = 16 (MINimum)

   No.   Row name        Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 OBJ                        16                             
     2 START[A]                    6             2               
     3 START[B]                   14             5               
     4 START[C]                   22             4               
     5 START[D]                    2            -0               
     6 START[E]                    0            -0               
     7 START[F]                   11             8               
     8 START[G]                   20             9               
     9 FINIS[A]                    5                           5 
    10 FINIS[B]                   14                          15 
    11 FINIS[C]                    7                           7 
    12 FINIS[D]                    2                           6 
    13 FINIS[E]                    0                           3 
    14 FINIS[F]                   11                          12 
    15 FINIS[G]                   20                          20 
    16 DA[A,B]                    31                          34 
    17 DA[A,C]                    23                          34 
    18 DA[A,D]                     4                          34 
    19 DA[A,E]                     6                          34 
    20 DA[A,F]                    34                          34 
    21 DA[A,G]                    25                          34 
    22 DA[B,C]                    31                          33 
    23 DA[B,D]                    12                          33 
    24 DA[B,E]                    14                          33 
    25 DA[B,F]                     3                          33 
    26 DA[B,G]                    33                          33 
    27 DA[C,D]                    20                          31 
    28 DA[C,E]                    22                          31 
    29 DA[C,F]                    11                          31 
    30 DA[C,G]                     2                          31 
    31 DA[D,E]                     2                          35 
    32 DA[D,F]                    30                          35 
    33 DA[D,G]                    21                          35 
    34 DA[E,F]                    28                          37 
    35 DA[E,G]                    19                          37 
    36 DA[F,G]                    30                          36 
    37 DB[A,B]                   -31                          -6 
    38 DB[A,C]                   -23                          -8 
    39 DB[A,D]                    -4                          -4 
    40 DB[A,E]                    -6                          -2 
    41 DB[A,F]                   -34                          -3 
    42 DB[A,G]                   -25                          -2 
    43 DB[B,C]                   -31                          -8 
    44 DB[B,D]                   -12                          -4 
    45 DB[B,E]                   -14                          -2 
    46 DB[B,F]                    -3                          -3 
    47 DB[B,G]                   -33                          -2 
    48 DB[C,D]                   -20                          -4 
    49 DB[C,E]                   -22                          -2 
    50 DB[C,F]                   -11                          -3 
    51 DB[C,G]                    -2                          -2 
    52 DB[D,E]                    -2                          -2 
    53 DB[D,F]                   -30                          -3 
    54 DB[D,G]                   -21                          -2 
    55 DB[E,F]                   -28                          -3 
    56 DB[E,G]                   -19                          -2 
    57 DB[F,G]                   -30                          -2 

   No. Column name       Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 start[A]                    6             0               
     2 start[B]                   14             0               
     3 start[C]                   22             0               
     4 start[D]                    2             0               
     5 start[E]                    0             0               
     6 start[F]                   11             0               
     7 start[G]                   20             0               
     8 pastdue[A]                  1             0               
     9 pastdue[B]                  0             0               
    10 pastdue[C]                 15             0               
    11 pastdue[D]                  0             0               
    12 pastdue[E]                  0             0               
    13 pastdue[F]                  0             0               
    14 pastdue[G]                  0             0               
    15 y[A,B]       *              1             0             1 
    16 y[A,C]       *              1             0             1 
    17 y[A,D]       *              0             0             1 
    18 y[A,E]       *              0             0             1 
    19 y[A,F]       *              1             0             1 
    20 y[A,G]       *              1             0             1 
    21 y[B,C]       *              1             0             1 
    22 y[B,D]       *              0             0             1 
    23 y[B,E]       *              0             0             1 
    24 y[B,F]       *              0             0             1 
    25 y[B,G]       *              1             0             1 
    26 y[C,D]       *              0             0             1 
    27 y[C,E]       *              0             0             1 
    28 y[C,F]       *              0             0             1 
    29 y[C,G]       *              0             0             1 
    30 y[D,E]       *              0             0             1 
    31 y[D,F]       *              1             0             1 
    32 y[D,G]       *              1             0             1 
    33 y[E,F]       *              1             0             1 
    34 y[E,G]       *              1             0             1 
    35 y[F,G]       *              1             0             1 

Integer feasibility conditions:

KKT.PE: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

KKT.PB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

End of output
