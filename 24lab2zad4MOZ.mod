#Dawid Leśniak
#ilosc zadan
param n>=1,integer;
#ilosc maszyn
param m>=1,integer;
#zbior zadan
set J:=1..n;
#zbior maszyn
set maszyny:=1..m;
#czas potrzebny do wykonania zadania
param p{J};
#graf relacji poprzedzania
param r{J,J},binary;
#maksymalny czas trwania zadan
param maxczas:=sum{i in J}p[i];
#zmienne decyzyjne ktora maszyna zostala wybrana dla danego zadania, czas rozpoczecia
#zmienna na ktorej maszynie wykonano zadanie
var zm{J,maszyny}>=0,binary;
#zmienna czas rozpoczecia zadania
var zt{J}>=0,<=maxczas,integer;
#czy zadanie j jest przed zadaniem k x[j,i]
var x{J,J},binary;
#funkcja celu, minimalizacja czasu na wykonanie wszystkich zadan
minimize czas: sum{i in J}(zt[i]);
#zadanie moze byc wykonane tylko na jednej maszynie
s.t. jedna_maszyna{i in J}:sum{j in maszyny}zm[i,j]=1;


s.t. A{i in J, k in J, z in maszyny:i<k}:zt[i]+p[i]-(1-zm[i,z])*maxczas <=zt[k]+(1-x[i,k]+1-zm[k,z])*maxczas;
s.t. B{i in J, k in J, z in maszyny:i<k}:zt[k]+p[k]-(1-zm[k,z])<=zt[i]+maxczas*(x[i,k]+1-zm[i,z]);
#warunek dla grafu relacji poprzedzania
s.t. C{i in J, k in J:i!=k}:zt[i]+p[i]<=zt[k]+maxczas*(1-r[i,k]);

solve;


#--------------------------------
for {mx in maszyny}{
printf "maszyna %d: ",mx;
	for{mczas in 0..maxczas}{
    	for{i in J}{
        	#jesli znaleziono max czas rozpoczecia dla danej maszyny
			printf if (zm[i,mx]&&(zt[i]==mczas)) then i else "";
            for{2..p[i]}{
            	printf if zm[i,mx]&&(zt[i]==mczas)then "=" else "";
            	}
		}
        
        printf if sum{ i in J:(zm[i,mx])&&(zt[i]<=mczas)&&(zt[i]+p[i]>mczas) }(1)>0 then "" else " ";
        
    }
printf "\n";
}


display max{i in J}(zt[i]+p[i]);

data;
#ilosc zadan
param n:=9;
#ilosc maszyn
param m:=3;
#czas trwania
param p:=
1	1
2	2
3	1
4	2
5	1
6	1
7	3
8	6
9	2;
#relacje poprzedzania r[i,j] czy i poprzedza j
 param r:	1	2	3	4	5	6	7	8	9:=
		1	0	0	0	1	0	0	0	0	0
		2	0	0	0	1	1	0	0	0	0
		3	0	0	0	1	1	0	0	0	0
		4	0	0	0	0	0	1	1	0	0
		5	0	0	0	0	0	0	1	1	0
		6	0	0	0	0	0	0	0	0	1
		7	0	0	0	0	0	0	0	0	1
		8	0	0	0	0	0	0	0	0	0
		9	0	0	0	0	0	0	0	0	0
;
end;
