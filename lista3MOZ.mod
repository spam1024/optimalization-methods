/*********************************************
 * OPL 12.6.0.0 Model
 * Author: Dawid Le�niak
 * Creation Date: 02-06-2015 at 00:22:07
 *********************************************/
//ilosc typow zasobow
int p=...;
// ilosc czynnosci
int n=...;
//zbior typow zasobow
range zasoby=1..p;
//zbior czynnosci
range czynnosci=1..n;
//limity zasobow
int N[zasoby]=...;
//zakres zasobow potrzebny do wyswietlenia danych
range rN=1..N[1];
//czas trwania czynnosci
int T[czynnosci]=...;
//zapotrzebowanie na zasoby
int r[czynnosci][zasoby]=...;
//graf z ograniczeniami kolejnosciowymi
//jesli w tablicy i,j jest 1 oznacza to ze zadanie i poprzedza zadanie j
int G[czynnosci][czynnosci]=...;
//maksymalny czas trwania wszystkich zadan tj gdyby wykonywane bylo jedno po drugim
int maxczas=sum(c in czynnosci)T[c];
range rmaxczas=0..maxczas;
//zmienne decyzyjne
dvar int czas_rozpoczecia[czynnosci] in rmaxczas;
//tablica binarna 1 gdy w danym czasie czynnosc jest wykonywana
dvar boolean tab_trwanie[czynnosci][rmaxczas];

//minimalizacja czasow rozpoczecia
minimize sum(c in rmaxczas,i in czynnosci)tab_trwanie[i][c]*c;

subject to{

//suma zuzywanych zasobow w czasie t nie moze przekraczac ograniczenia zasobow
	forall(j in rmaxczas)ograniczenie_zasobow: sum(k in czynnosci,i in zasoby)tab_trwanie[k][j]*r[k][i]<=sum(i in zasoby)N[i];
	
//dla czasow mniejszych od czasu rozpoczecia tablica jest zerowana, powyzej dowolna wartosc	
	forall(i in zasoby,j in rmaxczas, k in czynnosci)A:j+(1-tab_trwanie[k][j])*maxczas>=czas_rozpoczecia[k];
//?dla czasow wiekszych od czasu rozpoczecia+czas trwania tablica jest zerowana, ponizej dowolna wartosc	
	forall(i in zasoby,j in rmaxczas, k in czynnosci)B:j-(1-tab_trwanie[k][j])*maxczas<=czas_rozpoczecia[k]+T[k];
//suma okresow wynosi czas trwania, kazde zadanie musi zostac wykonane!
	forall( k in czynnosci)C:sum(j in rmaxczas)tab_trwanie[k][j] >=T[k]+1;
//zachowanie ustalonej kolejnosci
	forall(i in czynnosci, j in czynnosci:i!=j)graf_zaleznosci:G[i][j]*(czas_rozpoczecia[i]+T[i])<=czas_rozpoczecia[j];
}
