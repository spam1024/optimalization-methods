#Dawid Leśniak

#rozmiar tablicy Hilberta
param n, integer, >= 1; 

#zbior I
set I:={1..n};
#zbior J
set J:={1..n};

#tablica jednowymiarowa z wynikiem
var x{I} >=0;

#minimalizacja kosztu
minimize koszt{i in I}:x[i]*sum{j in J}1/(i+j-1);

#Ax=b, A to macierz hilberta 
s.t. warunki{i in I}: sum{j in J}(1/(i+j-1))*x[i] = sum{j in J}1/(i+j-1); 

solve;
#(x[i]-sum{j in J}1/(1+j-1));

display "blad wzgledny", sum{i in I}((x[i]-sum{j in J}1/(i+j-1))**2)/sum{j in J}1/(i+j-1);


data;
param n:=13;


end;
