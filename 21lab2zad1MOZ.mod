#Dawid Leśniak

param ilosc_podzialow>=1,integer;
set nr_podzialu:=1..ilosc_podzialow;
set rozmiar_desek;
#ilosc zamowionych desek danego typu
param zam7cali >=0,integer;
param zam5cali >=0,integer;
param zam3cale >=0,integer;

#sposoby podzialu deski 22 calowej na 3,5,7 cali
param podzial{nr_podzialu,rozmiar_desek},integer,>=0;

#zmienna decyzyjna, ile razy wybrano dany podzial deski
var x{nr_podzialu} >=0,<=120,integer;


#funkcja celu, minimalizacja straconych desek, strata podana w calach
minimize ilosc_straconych_cali: sum{i in nr_podzialu}(x[i]*podzial[i,"straty"]+7*x[i]*podzial[i,'siedem']+5*x[i]*podzial[i,'piec']+3*x[i]*podzial[i,'trzy'])-7*zam7cali-5*zam5cali-3*zam3cale;


#zamowienie
s.t. zamowionie_7_calowych:sum{i in nr_podzialu}x[i]*podzial[i,'siedem']>=zam7cali;
s.t. zamowionie_5_calowych:sum{i in nr_podzialu}x[i]*podzial[i,'piec']>=zam5cali;
s.t. zamowionie_3_calowych:sum{i in nr_podzialu}x[i]*podzial[i,'trzy']>=zam3cale;


data;
param ilosc_podzialow:=41;
set rozmiar_desek:=siedem piec trzy straty;

#zamowienie na deski
param zam7cali:=110;
param zam5cali:=120;
param zam3cale:=80;

#sposoby podzialu deski 22 calowej na 3,5,7 cali
param podzial:	siedem	piec	trzy	straty:=	
	1	2 	1 	1 	0
	2	1 	3 	0 	0
	3	1 	0 	5 	0
	4	0 	2	4	0
	5	1 	1 	3 	1
	6	3 	0	0	1
	7	0 	3 	2	1
	8	0 	0 	7	1
	9	2 	0 	2 	2
	10	1 	2 	1	2
	11	0 	1 	5	2
	12	0 	4	0	2
	13	0 	2 	3	3
	14	1 	0	4	3	
	15	2 	1	0 	3
	16	1 	1 	2 	4
	17	0 	3 	1	4
	18	0 	0 	6	4
	19	2 	0 	1 	5
	20	0 	1 	4	5
	21	1 	2 	0 	5
	22	0 	2 	2	6
	23	1 	0	3	6
	24	0 	3	0	7
	25	1	1 	1 	7
	26	0 	0 	5	7
	27	2 	0	0	8
	28	0 	1 	3	8
	29	1 	0 	2	9
	30	0 	2 	1	9
	31	1 	1 	0 	10
	32	0 	0 	4	10
	33	0 	1 	2	11
	34	0 	2	0	12
	35	1 	0	1	12	
	36	0 	0 	3	13
	37	1 	0	0 	15
	38	0 	0	2	16
	39	0 	1	0	17
	40	0 	1 	1	14
	41	0 	0	1	19	

;

end;
