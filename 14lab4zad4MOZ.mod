#Dawid Leśniak 
/*Czy istnieje taki rozkład zajęć, w którym wszystkie ćwicz
enia z przedmiotów obowiązkowych byłyby zgrupowane w trzech dniach - w poniedziałek, wtorek i czwartek oraz wszystkie odpowiadałyby preferencjom nie mniejszym niż 5 (suma wybranych grup w sr i pt=0 && pref wybranego przedmiotu>=5)
#istnieje: PN 
chemia mineralow  grI  8-10 
chemia organiczna grII 10:30-12 
algebra 	  grI  13-15 
CZW
analiza 	  grIV 8-10
fizyka		  grIV 17-20
*/

# zbior w ktorym znajduja sie wszystkie przedmioty
set przedmioty;
#zbior w ktorym znajduja sie wszystkie tygodnie
set tygodnie;

#grupy n-ilosc grup
param n >0,integer;
#zbior z grupami
set grupy:=1..n;

#tablice z informacjami
#w jaki dzien tygodnia odbywaja sie zajecia, kluczami sa nr grupy i nazwa przedmiotu, zwraca dzien tygodnia
param tab_tyg{grupy,przedmioty},symbolic;
#o ktorej godzinie zaczynaja sie zajecia, kluczami sa nr grupy i nazwa przedmiotu, godzine rozpoczecia
param tab_godz{grupy,przedmioty};
#czas trwania zajec, kluczem jest przedmiot, tablica zwraca czas trwania liczbe zmiennoprzecinkowa
param tab_czasTrwania{przedmioty},>0;

#preferencje grup, kluczami sa numer grupy oraz nazwa przedmiotu, zwracana jest wartosc preferencji
param tab_pref{grupy,przedmioty};

#zmienna odpowiadajaca wyborowi cwiczen, kluczami sa numer grupy i nazwa przedmiotu, tablica binarna, komorki z wartoscia 1 to wybrane grupy
var wybor{grupy, przedmioty},binary;

#zmaksymalizacja wyboru pod katem preferencji, suma preferencji wybranych grup powinna miec jak najwyzsza wartosc
maximize max_preferencje:sum{i in grupy,j in przedmioty} wybor[i,j]*tab_pref[i,j];

#warunki
#maksymalnie 4 godziny zajec kazdego dnia, suma godzin zajec danego dnia musi byc mniejsza od 4
s.t. max4h_dziennie {t in tygodnie}:sum{i in grupy, j in przedmioty:tab_tyg[i,j]==t}(wybor[i,j]*tab_czasTrwania[j])<=4;

#codziennie pomiedzy 12 a 14 godzina wolna na obiad, 
#odrzucono wszystkie przedmioty ktore pokrywaja sie z godzina przeznaczona na obiad
s.t. obiad{t in tygodnie}:sum{i in grupy, j in przedmioty:tab_tyg[i,j]==t} if (tab_godz[i,j]<=12) && (tab_godz[i,j]+tab_czasTrwania[j]>=14) then wybor[i,j]=0;
#suma wszystkich czasow ktore nachodza na ten przedzial musi byc mniejsza niz 1 poniewaz ten przedzial obejmuje 2h a 1h zarezerwowana jest na obiad
s.t. obiad2{t in tygodnie}:sum{i in grupy, j in przedmioty:(tab_tyg[i,j]==t)&&((tab_godz[i,j]<14&&tab_godz[i,j]>=12)||((tab_godz[i,j]+tab_czasTrwania[j])<=14)&&((tab_godz[i,j]+tab_czasTrwania[j])>12))}wybor[i,j]*(max(14,tab_godz[i,j]+tab_czasTrwania[j])-min(12,tab_godz[i,j]) )<=1;

#conajmniej raz w tygodniu trening, treningi: pn 13-15 sr11-13, 13-15
#nie jest wymagany warunek, zawsze w sr 13-15 jest wolny czas, nie ma zadnych zajec o tej porze


#kazdy przemiot moze byc wybrany tylko raz
s.t. przedmiot_tylko_raz{j in przedmioty}: sum{i in grupy}wybor[i,j] = 1; 


#godziny wybranych grup nie moga sie nakladac
s.t. nakladanieGodzin{i in grupy,i2 in grupy,j in przedmioty,j2 in przedmioty:(j!=j2)&&(tab_tyg[i,j]==tab_tyg[i2,j2])&&(tab_godz[i,j]<=tab_godz[i2,j2])}: if (tab_godz[i2,j2]-(tab_godz[i,j]+tab_czasTrwania[j]))<=0 then wybor[i2,j2] =0 ;



data;
# zbior w ktorym znajduja sie wszystkie przedmioty
set przedmioty:=algebra analiza fizyka chemia_mineralow chemia_organiczna;

#zbior w ktorym znajduja sie wszystkie tygodnie
set tygodnie:= poniedzialek wtorek sroda czwartek piatek;


#ilosc grup
param n:=4;

#tablice z informacjami
#w jaki dzien tygodnia odbywaja sie zajecia, kluczami sa nr grupy i nazwa przedmiotu, zwraca dzien tygodnia
param tab_tyg:  algebra      analiza      fizyka   chemia_mineralow chemia_organiczna:= 
	      1	poniedzialek poniedzialek wtorek   poniedzialek     poniedzialek
	      2 wtorek       wtorek       wtorek   poniedzialek     poniedzialek
	      3 sroda        sroda        czwartek czwartek         piatek
	      4 sroda        czwartek     czwartek piatek           piatek
;

#o ktorej godzinie zaczynaja sie zajecia, kluczami sa nr grupy i nazwa przedmiotu, godzine rozpoczecia
param tab_godz: algebra analiza fizyka chemia_mineralow chemia_organiczna:= 
	      1	13 13 8 8 9
	      2 10 10 10 8 10.5
	      3 10 11 15 13 11
	      4 11 8 17 13 13
;

#czas trwania zajec, kluczem jest przedmiot, tablica zwraca czas trwania liczbe zmiennoprzecinkowa
param tab_czasTrwania:= algebra 2 analiza 2 fizyka 3 chemia_mineralow 2 chemia_organiczna 1.5;

#preferencje grup, kluczami sa numer grupy oraz nazwa przedmiotu, zwracana jest wartosc preferencji
param tab_pref: algebra analiza fizyka chemia_mineralow chemia_organiczna:= 
	      1	5       4 	3 	10 		0
	      2 4 	4 	5 	10 		5
	      3 10 	5 	7 	7 		3
	      4 5 	6 	8 	5 		4
;


end;
