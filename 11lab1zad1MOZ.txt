Problem:    11lab1zad1MOZ
Rows:       26
Columns:    13
Non-zeros:  26
Status:     OPTIMAL
Objective:  koszt[1] = 3.180133755 (MINimum)

   No.   Row name   St   Activity     Lower bound   Upper bound    Marginal
------ ------------ -- ------------- ------------- ------------- -------------
     1 koszt[1]     B        3.18013                             
     2 koszt[2]     B        2.25156                             
     3 koszt[3]     B        1.81823                             
     4 koszt[4]     B         1.5474                             
     5 koszt[5]     B        1.35622                             
     6 koszt[6]     B        1.21177                             
     7 koszt[7]     B        1.09774                             
     8 koszt[8]     B        1.00488                             
     9 koszt[9]     B       0.927502                             
    10 koszt[10]    B       0.861845                             
    11 koszt[11]    B       0.805323                             
    12 koszt[12]    B       0.756081                             
    13 koszt[13]    B       0.712747                             
    14 warunki[1]   NS       3.18013       3.18013             =             1 
    15 warunki[2]   NS       2.25156       2.25156             =         < eps
    16 warunki[3]   NS       1.81823       1.81823             =         < eps
    17 warunki[4]   NS        1.5474        1.5474             =         < eps
    18 warunki[5]   NS       1.35622       1.35622             =         < eps
    19 warunki[6]   NS       1.21177       1.21177             =         < eps
    20 warunki[7]   NS       1.09774       1.09774             =         < eps
    21 warunki[8]   NS       1.00488       1.00488             =         < eps
    22 warunki[9]   NS      0.927502      0.927502             =         < eps
    23 warunki[10]  NS      0.861845      0.861845             =         < eps
    24 warunki[11]  NS      0.805323      0.805323             =         < eps
    25 warunki[12]  NS      0.756081      0.756081             =         < eps
    26 warunki[13]  NS      0.712747      0.712747             =         < eps

   No. Column name  St   Activity     Lower bound   Upper bound    Marginal
------ ------------ -- ------------- ------------- ------------- -------------
     1 x[1]         B              1             0               
     2 x[2]         B              1             0               
     3 x[3]         B              1             0               
     4 x[4]         B              1             0               
     5 x[5]         B              1             0               
     6 x[6]         B              1             0               
     7 x[7]         B              1             0               
     8 x[8]         B              1             0               
     9 x[9]         B              1             0               
    10 x[10]        B              1             0               
    11 x[11]        B              1             0               
    12 x[12]        B              1             0               
    13 x[13]        B              1             0               

Karush-Kuhn-Tucker optimality conditions:

KKT.PE: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

KKT.PB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

KKT.DE: max.abs.err = 0.00e+00 on column 0
        max.rel.err = 0.00e+00 on column 0
        High quality

KKT.DB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

End of output
