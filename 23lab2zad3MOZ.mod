#Dawid Leśniak

#ilosc zadan
param n>0,integer;
#zbior zadan
set J:=1..n;
#czas potrzebny na wykonanie zadania
param p{J}>0,integer;
#czas trwania wszystkich prac
param czas_prac:=sum{i in J}p[i];
#waga zadania
param w{J}>0,integer;
#moment gotowosci zadania przed ktorym zadanie nie moze zostac wykonane
param r{J}>0,integer;
#moment rozpoczecia zadania j t[1ktore zadanie rozpoczeto,2czas rozpoczecia]
var t{J}>=0, integer;
#czy praca j jest wykonywana przed k x[j,k]
var x{J,J},binary;
#maksymalna waga
param max_waga:=max{i in J}(w[i]);
#max czas rozpoczecia + suma wszystkich czasow trwania
param max_koniec := (max {k in J} r[k] ) + sum{k in J} p[k];


#tworzenie harmonogramu, waga zostaje odwrocona tj max element+1-wj
minimize harmonogram:sum{i in J,j in J:(i!=j)}(w[i])*t[i];
#czas rozpoczecia musi byc wiekszy od czasu gotowosci
s.t. rozpoczecie_gotowosc{i in J}:t[i]>=r[i];

#czasy nie moga sie nakladac, czas rozpoczecia nastepnego zadania jest wieksza od czasu rozpoczecia poprzedniego + czas wykonywania
#jesli tj jest przed tk wtedy czas rozpoczecia+czas trwania musi byc mniejszy od czasu rozpoczecia nastepnej pracy, w przeciwnym razie warunek zawsze spelniony 
#najpierw praca j potem k? tak= j+czas pracy <= k nie=warunek zawsze spelniony dla j<k
s.t. gorne{j in J, k in J : j < k}:t[j] + p[j] <= t[k] + max_koniec*(1-x[j,k]);
#podobnie jak powyzszy warunek lecz dla liczb wiekszych
s.t. dolne{j in J, k in J : j < k}:t[k]+p[k]<=t[j]+max_koniec*x[j,k];
#powyzsze dwa warunki dodatkowo eliminuja mozliwosc w ktorej j nie stoi przed k i k nie stoi przed j tj zaczynaja sie w tym samym momencie


data;
#ilosc zadan
param n:=10;
#czas potrzebny na wykonanie zadania
param p:=
1	10
2	5
3	4
4	6
5	4
6	6
7	7
8	10
9	3
10	8
;
#waga zadania
param w:=
1	8
2	9
3	10
4	8
5	8
6	6
7	7
8	4
9	1
10	2
;
#moment gotowosci zadania przed ktorym zadanie nie moze zostac wykonane
param r:=
1	7
2	7
3	10
4	3
5	2
6	2
7	9
8	2
9	9
10	1
;


end;
